import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Home from "./Containers/Home.js";
import Header  from "./Components/Header";
import Footer  from "./Components/Footer";
import SingleLaunch from "./Containers/SingleLaunch";
import LaunchDetail from "./Components/LaunchDetail";
import { connect } from "react-redux";
import Event from "./Containers/Event";

class Welcome extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: "Mike" };
  }
  handleClick = (el) => {
    console.log(el);
  };

  render() {
      return (
      <div style={{background: `url(${this.props.background}) no-repeat, rgba(24, 27, 72, 1)`}}>
        <div style={{background: 'rgba(24,27,72, 0.4'}}>
        <Router>
        <Header />
          <div>
            <Switch>
              <Route path={`/single-launch/:launchId`}><SingleLaunch /></Route>
              <Route path={`/detailed/:launchId`}><LaunchDetail /></Route>
              <Route path={`/event/:eventId`}><Event /></Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </div>
          <Footer />
        </Router>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return ({background: state.background})
};
export default connect(mapStateToProps)(Welcome);
