import { makeStyles } from "@material-ui/core/styles";
import Title from "../Components/Texts/Title";
import Description from "../Components/Texts/Description";
import CustomButton from "../Components/CustomButton";
import Slider from "../Components/Slider";
import LaunchList from "../Components/LaunchList.js";
import { Divider } from "@material-ui/core";

const useStyles = makeStyles( theme => ({
  banner: {
    paddingTop: "230px",
    [theme.breakpoints.down("md")]: {
      paddingTop: "150px",
    },
    [theme.breakpoints.down("sm")]: {
      paddingTop: "80px",
    },
  },
  bannerWrapper: {
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "center",
    flexDirection: "column",
  },
  content: {
    marginTop: '230px',
    maxWidth: '1440px',
    padding: '120px 130px',
    background: '#1C2056',
    margin: '230px auto 40px',
    [theme.breakpoints.down("md")]: {
      padding: '75px 60px',
      margin: '130px auto 40px'
    },
    [theme.breakpoints.down("sm")]: {
      padding: '50px 20px',
      margin: '75px auto 20px'
    }
  },
  contentWrapper:{
      width: '100%',
  }
}));

function Home() {
  const classes = useStyles();
  return (
    <main>
      <div className={classes.banner}>
        <div className={"container"}>
          <div className={classes.bannerWrapper}>
            <Title
              text={"Upcoming Spaceflight Launches"}
              align={'left'}
              maxWidth={'900px'}
            />
            <Description
              text={
                "View all launches available - including launches from the past and utilize powerful search filters."
              }
              align={'left'}
              maxWidth={'600px'}
            />
            <CustomButton
              text={"Show All Launches"}
              to={'#launches'}
              anchor
              align={'left'}
            />
          </div>
        </div>
      </div>
      <div className={classes.content}>
        <div className={"container"}>
            <div id="launches" className={classes.contentWrapper}>
                <Slider/>
                <Divider light={true} variant='fullWidth'/>
                <LaunchList />
            </div>
        </div>
      </div>
    </main>
  );
}
export default Home;
