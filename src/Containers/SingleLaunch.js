import { useParams } from "react-router";
import { useEffect, useState } from "react";
import GoogleMapReact from 'google-map-react';
import { makeStyles } from "@material-ui/core/styles";
import Title from "../Components/Texts/Title";
import Subtitle from "../Components/Texts/Subtitle";
import ItemName from "../Components/Texts/ItemName";
import VideoBlock from '../Components/VideoBlock'
import Description from "../Components/Texts/Description";
import EventDate from '../Components/Texts/EventDate'
import Button from '../Components/CustomButton'
import useRequestToSingleLaunch from '../hooks/SingleLaunchHook'
import { CHANGE_BACKGROUND_IMG } from "../store/actions";
import { useDispatch } from "react-redux";
import { DEFAULT_DATA } from "../consts";
import Countdown from '../Components/Countdown'


const useStyles = makeStyles(theme => ({
  banner: {
    paddingTop: "230px",
    [theme.breakpoints.down("md")]: {
      paddingTop: "150px",
    },
    [theme.breakpoints.down("sm")]: {
      paddingTop: "80px",
    },
  },
  bannerWrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    marginTop: "230px",
    maxWidth: "1440px",
    padding: "120px 130px",
    background: "#1C2056",
    margin: "230px auto 40px",
    [theme.breakpoints.down("md")]: {
      padding: '75px 60px',
      margin: '130px auto 40px'
    },
    [theme.breakpoints.down("sm")]: {
      padding: '50px 20px',
      margin: '75px auto 20px'
    }
  },
  contentWrapper: {
    width: "100%",
    maxWidth: '790px',
    margin: '0 auto',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  countdown: {
    marginTop: '40px',
    maxWidth: '780px',
    width: '100%',
    background: '#4a00e0',
    padding: '50px 15px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'monospace !important',
    '& span': {
      margin: "0 10px",
    }
  },
  infoList: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    columnGap: '30px', 
    [theme.breakpoints.down("md")]: {
      flexWrap: 'wrap'
    },
  },
  map: {
    width: '100%',
    marginTop: '100px',
    height: '500px'
  }
}));

function SingleLaunch() {
  const classes = useStyles();
  const location = useParams();
  const getTheLaunchInfo = useRequestToSingleLaunch(location)
  const [state, setState] = useState({});
  const [coords, setCoords] = useState([59.938043, 30.337157])
  const dispatch = useDispatch();

  useEffect(() => {
    setState(getTheLaunchInfo)
    dispatch(CHANGE_BACKGROUND_IMG(getTheLaunchInfo?.image_url || DEFAULT_DATA.BACKGROUND))
  }, [getTheLaunchInfo, dispatch])
 
 
  useEffect(() => {
    if(state?.pad){
      setCoords([state?.pad.latitude, state?.pad.longitude])
    }
  }, [state])
  

  return (
    <main>
      <div className={classes.banner}>
        <div className={"container"}>
          <div className={classes.bannerWrapper}>
            <Title text={state?.name} align={"center"} maxWidth={"900px"} />
            <ItemName text={'Go for Launch'} />
            <Countdown state={state} />
          </div>
        </div>
      </div>
      <div className={classes.content}>
        <div className={["container", classes.container].join(' ')}>
            <div className={classes.contentWrapper}>
              <VideoBlock />
              <Subtitle text={"Overview"} marginTop='100px' fontFamily='Montserrat-Bold'/>
              <Description color='#FFFFFF' text={`Destination: ${state?.mission && state.mission.orbit}`} marginTop='20px'/>
              <Description color='#FFFFFF' text={`Mission: ${state?.mission && state.mission.type}`} marginTop='5px'/>
              <div className={classes.infoList}>
                <EventDate text={state?.mission && state.mission.type} gradient/>
                <EventDate text={state?.mission && state.mission.orbit} gradient/>
                <EventDate text={state?.rocket && state.rocket.configuration.family} gradient/>
              </div>
              <Description text={state?.mission && state.mission.description} marginTop='30px'/>
              <Subtitle text={state?.name} marginTop='100px' fontFamily='Montserrat-Bold'/>
              <Description color='#FFFFFF' text={`Destination: ${state?.mission && state.mission.orbit}`} marginTop='20px'/>
              <Description color='#FFFFFF' text={`Mission: ${state?.mission && state.mission.type}`} marginTop='5px'/>
              <Description text={state?.mission && state.mission.description} marginTop='30px'/>
              <Button text='See Rocket Details' link={`/detailed/${state?.id}`} params={state}/>
            </div>
            <div className={classes.map}>
              <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyCVLCyLx6ZxcK0mpCXcRuoBQeH-16rQHDo' }}
                center={coords}
                defaultZoom={14}
              />
            </div>
        </div>
      </div>
    </main>
  );
}
export default SingleLaunch;
