import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core";
import useRequestToSingleEvent from "../hooks/SingleEventHook";
import { useDispatch } from "react-redux";
import { CHANGE_BACKGROUND_IMG } from "../store/actions";
import { DEFAULT_DATA } from "../consts";
import { useParams } from "react-router";
import Title from "../Components/Texts/Title";
import EventDate from "../Components/Texts/EventDate";
import Description from "../Components/Texts/Description";
import Button from "../Components/CustomButton";
import VideoBlock from "../Components/VideoBlock";
import Subtitle from "../Components/Texts/Subtitle";
import ItemName from "../Components/Texts/ItemName";
import Slider from "../Components/Slider";
import blank_img from "../images/img.jpg";

const useStyles = makeStyles(theme => ({
  banner: {
    paddingTop: "230px",
    [theme.breakpoints.down("md")]: {
        paddingTop: "150px",
      },
      [theme.breakpoints.down("sm")]: {
        paddingTop: "80px",
      },
  },
  contentWrapper: {
    width: "100%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
  content: {
    marginTop: "230px",
    maxWidth: "1440px",
    padding: "120px 130px",
    background: "#1C2056",
    margin: "230px auto 40px",
    [theme.breakpoints.down("md")]: {
        padding: '75px 60px',
        margin: '130px auto 40px'
      },
      [theme.breakpoints.down("sm")]: {
        padding: '50px 20px',
        margin: '75px auto 20px'
      }
  },
  descriptionBox: {
    marginTop: "100px",
    display: "grid",
    gridTemplateColumns: "repeat(2, 1fr)",
    gap: "20px",
    [theme.breakpoints.down("md")]: {
        marginTop: '50px'
      },
      [theme.breakpoints.down("sm")]: {
        marginTop: '30px'
      }
  },
  image: {
    width: "100%",
  },
  feature: {
    marginTop: "10px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    "&:first-of-type": {
      marginTop: "30px",
    },
  },
}));
function Event() {
  const classes = useStyles();
  const location = useParams();
  const getTheEventInfo = useRequestToSingleEvent(location);
  const [state, setState] = useState({});
  const [expeditions, setExpeditions] = useState({})
  const dispatch = useDispatch();
  useEffect(() => {
    setState(getTheEventInfo);
    dispatch(
      CHANGE_BACKGROUND_IMG(
        getTheEventInfo?.feature_image || DEFAULT_DATA.BACKGROUND
      )
    );
  }, [getTheEventInfo, dispatch]);
  useEffect(() => {
    setExpeditions(state?.expeditions || {})
  }, [state])
  return (
    <main>
      <div className={classes.banner}>
        <div className={"container"}>
          <div className={classes.contentWrapper}>
            <Title text={state?.name} />
            <EventDate text={state?.date} />
            <Description text={state?.description} />
            <Button to="/" text="Read On Site" />
          </div>
        </div>
      </div>

      <div className={classes.content}>
        <div className={"container"}>
          <div className={classes.contentWrapper}>
            <VideoBlock
              id={
                state?.video_url &&
                state?.video_url.replace(/(\S+)(v=)(\S+)/g, "$3")
              }
            />
            <div>
              <Subtitle text="Related Information" marginTop="50px" />
               <div className={classes.descriptionBox}>
                <img
                  src={expeditions[0] ? expeditions[0].spacestation.image_url : blank_img}
                  alt={state?.name}
                  className={classes.image}
                />
              
                <div>
                  <ItemName
                    text={expeditions[0] && expeditions[0].spacestation.name}
                    align="left"
                    marginTop="0"
                  />
                  <EventDate
                    text={state?.date}
                    width="300px"
                    marginTop="10px" 
                  />
                  <div className={classes.feature}>
                    <Description
                      text={"Destination:"}
                      marginTop="0"
                      marginRight="5px"
                      color="#fff"
                      weight="bold"
                    />
                    <Description
                      text={expeditions[0] && expeditions[0].spacestation?.orbit}
                      marginTop="0"
                      color="#fff"
                    />
                  </div>
                  <div className={classes.feature}>
                    <Description
                      text={"Mission:"}
                      marginTop="0"
                      marginRight="5px"
                      color="#fff"
                      weight="bold"
                    />
                    <Description
                      text={expeditions[0] && expeditions[0].name}
                      marginTop="0"
                      color="#fff"
                    />
                  </div>
                  <Description
                    text={state?.description}
                    color="#fff"
                    align="left"
                  />
                </div>
              </div>
            </div>
            <Slider/>
          </div>
        </div>
      </div>
    </main>
  );
}

export default Event;
