export const CHANGE_LOADER_STATUS = payload => dispatch => {
    dispatch({
        type: "CHANGE_LOADER_STATUS",
        payload,
    })
};
export const CHANGE_BACKGROUND_IMG = payload => dispatch => {
    dispatch({
        type: 'CHANGE_BACKGROUND_IMG',
        payload,
    })
}