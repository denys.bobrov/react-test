import { ACTION_TYPES } from "../consts";
import DEFAULT_BG from "../images/bg-image.jpg"
//Initial Redux State goes here
const initialState = {
    loading: false,
    background: DEFAULT_BG,
};


const reducers = (state = initialState, action) => {
    switch (action.type) {
        //Add come reducers here
        case ACTION_TYPES.CHANGE_LOADER_STATUS :
            const newStore = {
                ...state,
                loading: action.payload
            }
            return newStore;
        case ACTION_TYPES.CHANGE_BACKGROUND_IMG :
            const newBackground = {
                ...state,
                background: action.payload
            }
            return newBackground;
        default :
            return state;
    }
};

export default reducers;
