import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "swiper/swiper-bundle.css";
import App from "./App";
import store from "./store";
import { Provider } from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import MontserratBold from "./fonts/Montserrat-Bold.woff";
import MontserratExtraBold from "./fonts/Montserrat-ExtraBold.woff";
import MontserratMedium from "./fonts/Montserrat-Medium.woff";
import MontserratRegular from "./fonts/Montserrat-Regular.woff";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import { createTheme } from "@material-ui/core";

const montserrat = {
  bold: {
    fontFamily: 'Montserrat-Bold',
    fontStyle: 'normal',
    fontDisplay: 'swap',
    fontWeight: 800,
    src: `
      local('Montserrat-Bold'),
      local('Montserrat-Bold'),
      url(${MontserratBold}) format('woff')
    `,
  },
  medium: {
    fontFamily: 'Montserrat-Medium',
    fontStyle: 'normal',
    fontDisplay: 'swap',
    fontWeight: 500,
    src: `
      local('Montserrat-Medium'),
      local('Montserrat-Medium'),
      url(${MontserratMedium}) format('woff')
    `,
  },
  regular: {
    fontFamily: 'Montserrat-Regular',
    fontStyle: 'normal',
    fontDisplay: 'swap',
    fontWeight: 400,
    src: `
      local('Montserrat-Regular'),
      local('Montserrat-Regular'),
      url(${MontserratRegular}) format('woff')
    `,
  },
  extraBold: {
    fontFamily: 'Montserrat-ExtraBold',
    fontStyle: 'normal',
    fontDisplay: 'swap',
    fontWeight: 900,
    src: `
      local('Montserrat-ExtraBold'),
      local('Montserrat-ExtraBold'),
      url(${MontserratExtraBold}) format('woff')
    `,
  },
}


const theme = createTheme({
  typography: {
    h1: {
      fontFamily: 'Montserrat-Bold',
      fontSize: '76px',
      lineHeight: '1.2',
      fontWeight: '800',
      color: '#f1ebff',
    },
    subtitle1: {
      fontFamily: 'Montserrat-ExtraBold',
      fontSize: '76px',
      lineHeight: '1.2',
      color: '#F1EBFF',
      fontWeight: '900'
    },
    caption: {
      fontFamily: 'Montserrat-Medium',
      fontSize: '54px',
      lineHeight: '1.2',
      fontWeight: '800',
      color: '#f1ebff',
    },
    body1: {
      fontFamily: 'Montserrat-Regular, sans-serif',
      fontSize: '26px',
      lineHeight: '1.2',
      fontWeight: 'bold',
      color: '#F1EBFF',
      fontStyle: 'italic'
    }
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {  
        '@font-face': [montserrat.bold, montserrat.extraBold, montserrat.regular, montserrat.medium],
      },
    },
  },
});
ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Provider store={store}>
        <App />
      </Provider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
