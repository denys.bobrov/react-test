import { makeStyles } from "@material-ui/core";
import ItemName from "./Texts/ItemName";
import Description from "./Texts/Description";
const useStyles = makeStyles(theme =>({
    listOfFeatures: {
        marginTop: '20px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: '20px',
        [theme.breakpoints.down("md")]: {
            gap : '10px',
            marginTop: '0'
          }
    }
}))

function DetailColumn({state}){
    const classes = useStyles();

    return(
        <>
            {state.icon}
            <ItemName text={state.title}/>
            <ul className={classes.listOfFeatures}>
                {state.list.map((item, key) => {
                    return (
                        <li>
                            <Description text={item.label} marginTop='0' color='#fff'  weight='600'/>
                            <Description text={item.value || '-'} marginTop='5px' weight='900'/>
                        </li>
                    )
                })}
            </ul>
        </>
    )
}

export default DetailColumn 