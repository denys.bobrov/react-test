import { useEffect, useState } from "react";
import { Typography, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    countdown: {
      marginTop: '40px',
      maxWidth: '780px',
      width: '100%',
      background: '#4a00e0',
      padding: '50px 15px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      fontFamily: 'monospace !important',
      '& span': {
        margin: "0 10px",
      },
      [theme.breakpoints.down("md")]: {
        fontSize: "20px",
        padding: '20px 10px'
      }
    },
  }));

function Countdown({state}) {
    const classes = useStyles();
    const [countdown, SetCountdown] = useState({});
    const [intervalId, setIntervalId] = useState(null);
    useEffect(() => {
        if (state?.window_start) {
          let startDate = new Date(state?.window_start.toString());
          function interval() {
            let currDate = new Date();
            let difference = startDate.getTime() - currDate.getTime();
            let counter = {};
            counter.days = Math.floor(difference / (1000 * 60 * 60 * 24));
            counter.hours = Math.floor((difference / (1000 * 60 * 60)) % 24);
            counter.minutes = Math.floor((difference / (1000 * 60)) % 60);
            counter.seconds = Math.floor((difference / 1000) % 60);
            counter.days = ("0" + counter.days).slice(-2);
            counter.hours = ("0" + counter.hours).slice(-2);
            counter.minutes = ("0" + counter.minutes).slice(-2);
            counter.seconds = ("0" + counter.seconds).slice(-2);
            if (difference > 0) {
              SetCountdown(counter);
            }
          }
          const int = setInterval(() => interval(), 1000);
          if (!intervalId) {
            setIntervalId(int);
          }
        }
        return () => {
          clearInterval(intervalId);
        };
      }, [state, intervalId]);

      return (
        countdown && <Typography variant='subtitle1' component='span' className={classes.countdown}>
        {Object.values(countdown).join(':')}
        </Typography>
      )
}
export default Countdown