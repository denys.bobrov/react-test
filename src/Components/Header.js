import React, { useEffect, useState } from "react";
import logo from "../images/Logo.svg";
import { Link, useLocation } from "react-router-dom";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    header: {
      width: "100%",
      background: "rgba(0, 0, 0, 0.2)",
    },
    link: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    logo: {
      position: "relative",
      top: "50px",
    },
    spaceBetween:{
      justifyContent: 'space-between'
    }
  });
function Header() {
  const classes = useStyles();
const location = useLocation();
const [homeLink, setHomeLink] = useState(true)

    useEffect(() => {
        if(location.pathname !== '/'){
            setHomeLink(false)
            return
        }
        return(
            setHomeLink(true)
        )
    }, [location.pathname])
  return (
    <header className={classes.header}>
      <div className={["container", !homeLink ? classes.spaceBetween : ''].join(' ')}>
          {!homeLink && (<Link to='/' className={classes.link}> <ArrowBackIcon /> Back to Home</Link>)}
        <img className={classes.logo} src={logo} alt={"Logo"}/>
      </div>
    </header>
  );
}
export default Header;
