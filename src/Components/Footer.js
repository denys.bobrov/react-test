import logo from "../images/Logo.svg";
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    footer: {
      width: "100%",
      padding: '80px 0'
    },
    logo: {
        maxWidth: '55px'
    },
    container: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        maxWidth: '1200px',
        padding: '0 15px',

    },
    copyright: {
        color: "#C0C0C0",
        font: '17px/28px Roboto, sans-serif'
    }
  });
function Footer() {
  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <div className={[classes.container, 'container'].join(' ')}>
        <img className={classes.logo} src={logo} alt={"Logo"}/>
        <p className={classes.copyright}>© 2021 Copyright</p>
      </div>
    </footer>
  );
}
export default Footer;
