import { makeStyles } from "@material-ui/core";
import React from "react";
import blank_img from "../images/img.jpg";
import EventDate from "./Texts/EventDate";
import ItemName from "./Texts/ItemName";


const useStyles = makeStyles({
  item: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "& div": {
      position: "relative",
      width: "100%",
      display: 'block',
    },
    "& div::before": {
      content: '""',
      paddingBottom: "65%",
      display: "block",
    },
    "& img": {
      position: "absolute",
      left: "0",
      top: "0",
      width: "100%",
      height: "100%",
      objectFit: "cover",
    },
  },
});

function Item({ data }) {
  const classes = useStyles();
  return (
    <div className={classes.item}>
      <div>
        <img
          alt={data.name}
          src={data.image_url ? data.image_url : blank_img}
        />
      </div>
      {data.window_start && <EventDate text={data.window_start.replace(/[a-zA-Z]/g, ' ')} transform={'translateY(-50%)'} marginTop={'0'} gradient={true}/>}
      <ItemName className={"title"} text={data.name} align={'center'} marginTop={'-5px'}/>
    </div>
  );
}

export default Item;
