import { makeStyles } from "@material-ui/core/styles";
import Title from "../Components/Texts/Title";
import Description from "../Components/Texts/Description";
import ItemName from "../Components/Texts/ItemName";
import EventDate from './Texts/EventDate'
import useRequestToSingleLaunch from '../hooks/SingleLaunchHook'
import { useState, useEffect } from "react";
import { useParams } from "react-router";
import { useDispatch } from "react-redux";
import { DEFAULT_DATA } from "../consts";
import { CHANGE_BACKGROUND_IMG } from "../store/actions";
import Family from "./icons/Family";
import Specification from "./icons/Specification";
import Payload from "./icons/Payload";
import DetailColumn from "./DetailColumn";

const useStyles = makeStyles(theme => ({
  banner: {
    paddingTop: "230px",
    [theme.breakpoints.down("md")]: {
      paddingTop: "150px",
    },
    [theme.breakpoints.down("sm")]: {
      paddingTop: "80px",
    },
  },
  bannerWrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    marginTop: "230px",
    maxWidth: "1440px",
    padding: "120px 130px",
    background: "#1C2056",
    margin: "230px auto 40px",
    [theme.breakpoints.down("md")]: {
      padding: '75px 60px',
      margin: '130px auto 40px'
    },
    [theme.breakpoints.down("sm")]: {
      padding: '50px 20px',
      margin: '75px auto 20px'
    }
  },
  contentWrapper: {
    width: "100%",
    maxWidth: '790px',
    margin: '0 auto',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  infoList: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    columnGap: '30px', 
    [theme.breakpoints.down("md")]: {
      flexWrap : 'wrap'
    }
  },
  listWrapper: {
    marginTop: '50px',
    display: 'grid',
    gridTemplateColumns: 'repeat(3, 1fr)',
    columnGap: '30px',
    width: '100%',
    [theme.breakpoints.down("md")]: {
      gridTemplateColumns : '1fr',
      rowGap: '40px'
    }
  },
  detailedList: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    rowGap: '20px'
  }
}));

function LaunchDetail(props) {
  const classes = useStyles();
  const location = useParams();
  const getTheLaunchInfo = useRequestToSingleLaunch(location)
  const [state, setState] = useState({});
  const [arrOfLists, setArrOfLists] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    setState(getTheLaunchInfo?.rocket?.configuration)
    dispatch(CHANGE_BACKGROUND_IMG(getTheLaunchInfo?.image_url || DEFAULT_DATA.BACKGROUND))
    setArrOfLists([
      {
        icon: <Family size="100"/>,
        title: "Family",
        list: [
          {
            label: 'Name',
            value: state?.name,
          },
          {
            label: 'Family',
            value: state?.family,
          },
          {
            label: 'Variant',
            value: state?.variant,
          },
          {
            label: 'Full Name',
            value: state?.full_name,
          },
          {
            label: 'Alias',
            value: state?.alias,
          },
        ],
      },
      {
        icon: <Specification size="100"/>,
        title: "Specifications",
        list: [
          {
            label: 'Minimum Stage',
            value: state?.min_stage,
          },
          {
            label: 'Max Stage',
            value: state?.max_stage,
          },
          {
            label: 'Length',
            value: state?.length,
          },
          {
            label: 'Diameter',
            value: state?.diameter,
          },
          {
            label: 'Launch Mass',
            value: state?.launch_mass,
          },
          {
            label: 'Thrust',
            value: state?.to_thrust,
          },
          {
            label: 'Apogee (Sub-Orbital)',
            value: state?.apogee,
          },
        ],
      },
      {
        icon: <Payload size="100"/>,
        title: "Payload Capacity",
        list: [
          {
            label: 'Launch Cost',
            value: `$${Math.round(Math.random() * 10000) * 10000}`,
          },
          {
            label: 'Low Earth Orbit',
            value: state?.leo_capacity,
          },
          {
            label: 'Geostationary Transfer Orbit',
            value: state?.gto_capacity,
          },
          {
            label: 'Direct Geostationary',
            value: '-',
          },
          {
            label: 'Sun-Synchronous Capacity',
            value: '-',
          },
        ],
      },
    ])
  }, [getTheLaunchInfo, dispatch, state])

  return (
    <main>
      <div className={classes.banner}>
        <div className={"container"}>
          <div className={classes.bannerWrapper}>
            <Title text={state?.name} align={"center"} maxWidth={"900px"} />
            <ItemName text={state?.family} />
            <Description color='#C0C0C0' text={state?.description} marginTop='5px'/>
          </div>
        </div>
      </div>
      <div className={classes.content}>
        <div className={["container", classes.container].join(' ')}>
            <div className={classes.contentWrapper}>
              <div className={classes.infoList}>
                <EventDate text={state?.variant} gradient/>
                <EventDate text={state?.launch_service_provider?.type} gradient/>
                <EventDate text={state?.launch_service_provider?.country_code} gradient/>
              </div>
            </div>
              <div className={classes.listWrapper}>
                {arrOfLists.map((item, index) => {
                  return(
                    <div className={classes.detailedList}>
                      <DetailColumn state={item} key={index}/>
                    </div>
                  )
                })}
            </div>
        </div>
      </div>
    </main>
  );
}
export default LaunchDetail;
