import { makeStyles } from "@material-ui/core/styles";
import YouTube from 'react-youtube';

const useStyles = makeStyles(theme => ({
  iframeWrapper: {
    width: "100%",
    position: "relative",
    display: "block",
    overflow: 'hidden',
    "&::before": {
      content: '""',
      paddingBottom: "62%",
      display: "block",
      width: '100%'
    },
    "& > div": {
      position: 'absolute',
      width: '100%',
      height: '100%',
      top: '0',
      left: '0'
    },
  },
  iframe: {
    position: "absolute",
    left: "50%",
    transform: 'translateX(-50%)',
    top: "0",
    width: "100%",
    height: "100%",
    objectFit: "cover",
  },
}));

function VideoBlock(props) {
  const classes = useStyles();
  return (
    <div className={classes.iframeWrapper}>
      <YouTube videoId={props.id ? props.id : 'sSiuW1HcGjA'} className={classes.iframe}/>
    </div>
  );
}

export default VideoBlock;
