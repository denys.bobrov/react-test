import { useState, useEffect, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import SwiperCore, { Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import ApiServiceInstance from "../service";
import blank_img from "../images/img.jpg";
import Subtitle from "./Texts/Subtitle";
import EventDate from "./Texts/EventDate";
import ItemName from "./Texts/ItemName";
import ArrowLeft from "./icons/ArrowLeft";
import ArrowRight from "./icons/ArrowRight";
import { Link } from "react-router-dom";

const useStyles = makeStyles( theme => ({
  sliderWrapper: {
    width: "100%",
  },
  sliderTitleAndButtons: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: "60px",
    marginTop: '50px',
    
    [theme.breakpoints.down("md")]: {
      marginBottom: '30px'
    }
  },
  sliderNavigation: {
    [theme.breakpoints.down("md")]: {
      display: 'none'
    }
  },
  sliderNavigationButton: {
    background: 'transparent',
    cursor: 'pointer',
    margin: '0 20px',
    '& path' : {
      transition: '0.3s ease'
    },
    '&:hover path' : {
      fill: '#4A00E0'
    }
  },
  image:{
    width: '100%'
  }
}));

function Slider() {
  const classes = useStyles();
  const [state, setState] = useState([]);
  const sliderRef = useRef();
  SwiperCore.use([Navigation]);

  useEffect(() => {
    async function fetchData() {
      const i = await ApiServiceInstance.getEvents({
        mode: "detailed",
        limit: 5,
      });
      setState(i);
    }
    fetchData();
  }, []);

  return (
    <div className={classes.sliderWrapper}>
      <div className={classes.sliderTitleAndButtons}>
        <Subtitle text={"Recent Events"}/>
        <div className={classes.sliderNavigation}>
          <button className={["prevButton", classes.sliderNavigationButton].join(' ')}>
            <ArrowLeft />
          </button>
          <button className={["nextButton", classes.sliderNavigationButton].join(' ')}>
            <ArrowRight />
          </button>
        </div>
      </div>
      <Swiper
        ref={sliderRef}
        className={classes.slider}
        spaceBetween={20}
        breakpoints={{
          1024: {
            slidesPerView: 3
          },
          480: {
            slidesPerView: 2
            
          },
          320: {
            slidesPerView: 1
          }
        }}
        navigation={{
          nextEl: ".nextButton",
          prevEl: ".prevButton",
        }}
        onSlideChange={() => console.log("slide change")}
      >
        {state?.length &&
          state.map((item, index) => {
            return (
              <SwiperSlide key={index}>
                <Link to={`/event/${item.id}`}>
                  <img
                    src={item.image_url ? item.image_url : blank_img}
                    alt={item.name}
                    className={classes.image}
                  />
                  <EventDate text={item.date.replace(/[a-zA-Z]/g, ' ')} gradient={false}/>
                  <ItemName text={item.name} align={'left'}/>
                </Link>
              </SwiperSlide>
            );
          })}
      </Swiper>
    </div>
  );
}

export default Slider;
