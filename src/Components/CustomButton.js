import { makeStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import Button from '@material-ui/core/Button';


const useStyles = makeStyles( theme => ({
    Button: {
        background: 'linear-gradient(93.72deg, #8E2DE2 9.41%, #4A00E0 86.1%)',
        borderRadius: '50px',
        padding: '30px 10px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#f1ebff',
        fontWeight: 'bold',
        font: '20px/1.1 Montserrat, sans-serif',
        maxWidth: '360px',
        width: '100%',
        marginTop: '40px',
        [theme.breakpoints.down("md")]: {
            padding: '20px 10px',
            marginTop: '30px',
            font: '17px/1.1 Montserrat, sans-serif',

        },
        [theme.breakpoints.down("sm")]: {
            padding: '15px 10px',
            marginTop: '20px',
            font: '15px/1.1 Montserrat, sans-serif',

        },
    }
  }));


function CustomButton(props){
    const classes = useStyles();
    const clickTrigger = () =>{
        if(props.anchor){
            document.querySelector(props.to).scrollIntoView({block: "start", behavior: "smooth"})
        }
    }
    return(
        <Button className={classes.Button} onClick={clickTrigger}>
            <Link to={props.link || '/'}>{props.text}</Link>
        </Button>
    )
}

export default CustomButton