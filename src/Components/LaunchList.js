import React, { useState, useEffect, useRef } from "react";
import ApiServiceInstance from "../service";
import Item from "./Item";
import { useDispatch } from "react-redux";
import { DEFAULT_DATA } from "../consts";
import { CHANGE_LOADER_STATUS, CHANGE_BACKGROUND_IMG } from "../store/actions";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core";
import Title from "./Texts/Title";
import InfiniteScroll from "react-infinite-scroll-component";
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  LaunchList: {
    marginTop: "100px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      marginTop: '50px'
    }
  },
  wrapper: {
    display: "grid",
    gridTemplateColumns: "repeat(2, 1fr)",
    columnGap: "20px",
    rowGap: "50px",
    marginTop: "40px",
    overflow: 'hidden !important',
    [theme.breakpoints.down("md")]: {
      gridTemplateColumns: '1fr',
      rowGap: '40px'
    }
  },
  LoaderWrapper: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    gridColumnStart: '1',
    gridColumnEnd: '3',
    [theme.breakpoints.down("md")]: {
      gridColumnEnd: '2',
    }
  },
  Loader: {
    marginTop: "40px",
  },
}));

function LaunchList() {
  const loader = useRef();
  const [state, setState] = useState([]);
  const dispatch = useDispatch();
  const [counter, SetCounter] = useState(10);
  const classes = useStyles();

  function fetchMoreData () {
    setTimeout(() => {
      SetCounter(counter + 10)
    }, 1500);
  };
  useEffect(() => {
    dispatch(CHANGE_BACKGROUND_IMG(DEFAULT_DATA.BACKGROUND))
  }, [dispatch])

  useEffect(() => {
    let mountedState = true;
    async function fetchData() {
      const list = await ApiServiceInstance.getLaunches({
        mode: "detailed",
        limit: counter,
      })
      if (mountedState) {
        dispatch(CHANGE_LOADER_STATUS(true));
        setState(list);
      }
    }
    fetchData();
    return () => {
      mountedState = false;
      dispatch(CHANGE_LOADER_STATUS(false));

    };
  }, [dispatch, counter]);
  
  return (
    <div className={classes.LaunchList}>
      <Title text={"Spaceflight Launches"} />
      <InfiniteScroll
          dataLength={state.length}
          next={fetchMoreData}
          hasMore={true}
          className={classes.wrapper}
          scrollThreshold='100%'
          loader={<div className={classes.LoaderWrapper}><CircularProgress
            color="primary"
            size="60px"
            ref={loader}
            className={classes.Loader}
          /></div>}
        >
          {state?.length ?
            state.map((el, index) => {
              return (
                  <Link to={`/single-launch/${el.id}`} key={index}>
                    <Item data={el}  />
                  </Link>
              );
            }) : ''}
        </InfiniteScroll>

    </div>
  );
}

export default LaunchList;
