import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    Description: ({align, weight, color, marginTop, maxWidth, marginRight }) => ({
        font: '17px/28px Montserrat, sans-serif',
        textAlign: align ? align : 'center',
        fontWeight: weight ? weight : 'normal',
        color: color ? color : '#C0C0C0',
        marginTop: marginTop ? marginTop : '30px',
        maxWidth: maxWidth ? maxWidth : '100%',
        marginRight: marginRight || '0',
        [theme.breakpoints.down("md")]: {
            font: '15px/25px Montserrat, sans-serif',
            marginTop: marginTop ? marginTop * 3 / 2 : '20px',
        },
        [theme.breakpoints.down("sm")]: {
            font: '13px/22px Montserrat, sans-serif',
            marginTop: marginTop ? marginTop / 2 : '15px',
        },
    })
  }));


function Description(props){
    const classes = useStyles(props);
    return(
        <p className={classes.Description}>{props.text}</p>
    )
}

export default Description