import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    ItemName: ({align, marginTop, normal}) => ({
        textAlign: align ? align : 'center',
        marginTop: marginTop ? marginTop : '15px',
        fontStyle: normal ? 'normal' : 'italic',
        width: '100%',
        [theme.breakpoints.down("md")]: {
            fontSize: '21px',
            marginTop: marginTop || '10px'
        },
        [theme.breakpoints.down("sm")]: {
            fontSize: '18px',
            marginTop: marginTop || '10px'
        }
    }),
  }));


function ItemName(props){
    const classes = useStyles(props);
    return(
        <Typography variant='body1' component='p' className={classes.ItemName}>{props.text}</Typography>
    )
}

export default ItemName