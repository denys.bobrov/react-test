import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
    EventDate: ({gradient, marginTop, transform, width}) => ({
        font: '18px/1 Montserrat, sans-serif',
        textAlign: 'center',
        fontWeight: '500',
        color: '#f1ebff',
        padding: '10px 30px',
        background: gradient ? 'linear-gradient(94.97deg, #8E2DE2 3.92%, #4A00E0 52.92%)' : '#4A00E0',
        borderRadius: '50px',
        marginTop: marginTop ? marginTop : '20px',
        transform: transform ? transform : 'none',
        width: width || 'auto',
        [theme.breakpoints.down("md")]: {
            font: '16px/1 Montserrat, sans-serif',
        }
    })
  }));

function EventDate(props){
    const classes = useStyles(props);
    return(
        <p className={classes.EventDate}>{props.text}</p>
    )
}

export default EventDate