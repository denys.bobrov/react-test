import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    title: ({align, marginTop, maxWidth}) =>({
        textAlign: align ? align : 'center',
        maxWidth: maxWidth ? maxWidth : '100%',
        marginTop: marginTop,
      [theme.breakpoints.down("md")]: {
        fontSize: "56px"
      },
      [theme.breakpoints.down("sm")]: {
        fontSize: "36px"
      },
    }) 
  }));


function Title(props){
    const classes = useStyles(props);
    return(
        <Typography variant='h1' component='h1' className={classes.title}>{props.text}</Typography>
    )
}

export default Title