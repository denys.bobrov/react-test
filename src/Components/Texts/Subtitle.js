import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    Subtitle: ({align, marginTop, maxWidth, fontFamily}) => ({
        textAlign: align ? align : 'center',
        maxWidth: maxWidth ? maxWidth : '100%',
        marginTop: marginTop || '0',
        fontFamily: fontFamily ? fontFamily : '',
        [theme.breakpoints.down("md")]: {
            fontSize: "40px", 
        },
        [theme.breakpoints.down("sm")]: {
            fontSize: "28px",
        },
    })
  }));


function Subtitle(props){
    const classes = useStyles(props);
    return(
        <Typography variant='caption' component='h3' className={classes.Subtitle}>{props.text}</Typography>
    )
}

export default Subtitle