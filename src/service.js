import axios from 'axios';
import {ENDPOINTS} from "./consts";


class ApiService {
  instance;

  constructor() {
    this.instance = axios.create(
        {
            baseURL: "https://spacelaunchnow.me/api/3.3.0/",
            timeout: 5000,
          }
    );
  }

  getLaunches = async (path) => {
    try {
      const result = await this.instance.get(ENDPOINTS.GET_LAUNCHES_ENDPOINT, {
        params: path
      });
      return result.data.results;
    } catch (e) {
      this.fallback(e);
      return []
    }
  };
  getEvents = async (path) => {
    try {
      const result = await this.instance.get(ENDPOINTS.GET_EVENTS_ENDPOINT, {
        params: path
      });
      return result.data.results;
    } catch (e) {
      this.fallback(e);
    }
  };
  getSingleLaunch = async (path) => {
    try {
      const single_launch_endpoint = ENDPOINTS.GET_SINGLE_LAUNCH_ENDPOINT + path.launchId
      const result = await this.instance.get(single_launch_endpoint);
      return result.data;
    } catch (e) {
      this.fallback(e);
    }
  }
  getSingleEvent = async (path) => {
    try {
      const single_event_endpoint = ENDPOINTS.GET_SINGLE_EVENT_ENDPOINT + path.eventId
      const result = await this.instance.get(single_event_endpoint);
      return result.data;
    } catch (e) {
      this.fallback(e);
    }
  }
  fallback = (e) => {
    console.error(e)
  };
}

const ApiServiceInstance = new ApiService();

export default ApiServiceInstance;
