import { useState, useEffect } from 'react';
import ApiServiceInstance from "../service"

function useRequestToSingleLaunch(location) {
    const [state, setState] = useState({});

    useEffect(() => {
        let mountedState = true;
        async function fetchData() {
          const singleLaunch = await ApiServiceInstance.getSingleLaunch(location);
          if (mountedState) {
            setState(singleLaunch);
          }
        }
        fetchData();
        return () => {
          mountedState = false;
        };
      }, [location]);
      return state
  }

export default useRequestToSingleLaunch;