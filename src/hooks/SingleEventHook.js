import { useState, useEffect } from 'react';
import ApiServiceInstance from "../service"

function useRequestToSingleEvent(location) {
    const [state, setState] = useState({});

    useEffect(() => {
        let mountedState = true;
        async function fetchData() {
          const singleEvent = await ApiServiceInstance.getSingleEvent(location);
          if (mountedState) {
            setState(singleEvent);
          }
        }
        fetchData();
        return () => {
          mountedState = false;
        };
      }, [location]);
      return state
  }

export default useRequestToSingleEvent;