import DEFAULT_BG from "../images/bg-image.jpg"

const ACTION_TYPES =  {
    CHANGE_LOADER_STATUS: 'CHANGE_LOADER_STATUS',
    CHANGE_BACKGROUND_IMG: 'CHANGE_BACKGROUND_IMG'
}
const ENDPOINTS =  {
    GET_LAUNCHES_ENDPOINT: '/launch/upcoming',
    GET_EVENTS_ENDPOINT: '/event/upcoming',
    GET_SINGLE_LAUNCH_ENDPOINT: '/launch/',
    GET_SINGLE_EVENT_ENDPOINT: '/event/'
}
const DEFAULT_DATA = {
    BACKGROUND : DEFAULT_BG
}
const constants = {ACTION_TYPES, ENDPOINTS, DEFAULT_DATA}


export {ACTION_TYPES, ENDPOINTS, DEFAULT_DATA};
export default constants;